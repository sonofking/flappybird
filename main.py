import pygame
from random import randrange

def accident(bird_x, bird_y, bird_w, bird_h,
             p_x, p_y, p_w, p_h):
    return bird_x + bird_w >= p_x \
           and bird_x + bird_w <= p_x + p_w \
           and bird_y + bird_h >= p_y \
           and bird_y + bird_h <= p_y + p_h

pygame.init()
# Параметры окна
width = 500
height = 500
window = pygame.display.set_mode((width, height))
# параметры птички
bird = pygame.image.load('bird.png')
x, y = width / 2, height / 2
# параметры препятствий
p1 = pygame.Surface((50, 400))
p2 = pygame.Surface((50, 400))
p_x = width
p1_y = randrange(-350, -50)
p2_y = randrange(250, 450)
speed = 0.3
delta = 250

# основной цикл
while True:
    for e in pygame.event.get():
        if e.type == pygame.QUIT:
            exit()

    if accident(x, y, 90, 84, p_x, p1_y, 50, 400):
        exit()
    if accident(x, y, 90, 84, p_x + delta, p2_y, 50, 400):
        exit()

    # описываем смещение препятствий
    p_x -= speed
    if p_x < -delta:
        p_x = width
        p1_y = randrange(-350, 0)
        p2_y = randrange(250, 450)
        delta = randrange(200, 400)
    # описываем движение птички
    y += 0.5
    key = pygame.key.get_pressed()
    if key[pygame.K_SPACE]: y -= 2
    # задаем параметры окна и наносим слои
    window.fill(pygame.Color('yellow'))
    window.blit(bird, (x, y))
    window.blit(p1, (p_x, p1_y))
    window.blit(p2, (p_x + delta, p2_y))
    pygame.display.update()
pygame.quit()




